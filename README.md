# Guide to productionizing a Hive query

Guide for Wikimedia Foundation's Product Analytics team to setting up a Hive query-based data pipeline with Airflow.

## Development

```
python3 -m http.server
```

Navigate to `http://localhost:8000`

## License

Refer to [licensing information](LICENSE.md) for details.
