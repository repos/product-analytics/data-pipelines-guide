<!DOCTYPE html>
<html class="no-js" lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <link rel="stylesheet" href="./styles/annotate.css" />
  <link rel="stylesheet" href="./styles/styles.css" />
  <script src="./scripts/annotate.js" defer></script>
  <meta property="og:locale" content="en_US" />
  <title>Guide to productionizing a Hive query</title>
  <meta name="description" content="Guide for Wikimedia Foundation's Product Analytics team to setting up a Hive query-based data pipeline with Airflow." />
  <meta name="author" content="Wikimedia Foundation" />

</head>

<body>
  <div class="title-block" id="top">
    <h1>Guide to productionizing a Hive query</h1>
    <div class="byline">
      <a href="https://wikitech.wikimedia.org/wiki/Data_Engineering">Data Engineering</a>
      &amp; <a href="https://www.mediawiki.org/wiki/Product_Analytics">Product Analytics</a>
    </div>
  </div>
  <div class="intro">
    <p>
      This is meant to be a guide to setting up an ETL job (a data pipeline) based on a Hive query.
      The queries to be executed are in <a href="https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines"><code>repos/product-analytics/data-pipeline</code></a>.
      The jobs executing those queries are in <a href="https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags"><code>repos/data-engineering/airflow-dags</code></a>.
      Deployment instructions are documented <a href="https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Airflow/Instances#analytics_product">on Wikitech</a>.
    </p>
    <p>The work to enable Jupyter notebook-based pipelines is in-progress.</p>
    <p>
      Some of the text in this guide comes from the <a href="https://wikitech.wikimedia.org/wiki/Data_Engineering/Systems/Airflow/Developer_guide">Airflow developer guide on Wikitech</a>, available under the <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.en">Creative Commons Attribution-ShareAlike License</a> like the rest of the text in this guide.
    </p>
    <p>
      <h2 class="toc-heading">Table of Contents</h2>
      <ol class="toc-list" role="list">
        <li><a href="#query">Query</a></li>
        <li><a href="#data-dependencies">Data Dependencies</a></li>
        <li>
          <a href="#job-specification">Job Specification</a>
          <ol role="list">
            <li><a href="#preamble">Preamble</a></li>
            <li><a href="#properties">Properties</a></li>
            <li><a href="#dag">DAG</a></li>
          </ol>
        </li>
        <li><a href="#unit-test">Unit Test</a></li>
      </ol>
    </p>
  </div>
  <main class="maincontent">
    <article class="article">
      <section class="group first">
        <div class="content quote">
          <h2 id="query">Query</h2>
          <p>
            This Hive query is a simplified version of
            <a href="https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines/-/blob/main/wikipedia_preview/generate_wikipedia_preview_stats_daily.hql">generate_wikipedia_preview_stats_daily.hql</a>.
          </p>
          <pre><code class="language-python">
--
-- Populates the wikipedia_preview_stats table daily,
-- filtering webrequest data and aggregating it into interesting dimensions.
--
-- Parameters:
--     source_table         -- Fully qualified table name of the source webrequest data.
--     destination_table    -- Fully qualified table name for the wikipedia preview stats.
--     year                 -- Year of the partition to process.
--     month                -- Month of the partition to process.
--     day                  -- Day of the partition to process.
--
-- <mark data-annotation-id="1" aria-details="usage">Usage</mark>:
--     spark3-sql -f generate_wikipedia_preview_stats_daily.hql \
--         -d <mark data-annotation-id="2" aria-details="table-naming">source_table=wmf.webrequest</mark> \
--         -d destination_table=wmf_product.wikipedia_preview_stats \
--         -d year=2023 \
--         -d month=5 \
--         -d day=21
--

INSERT OVERWRITE TABLE <b>${destination_table}</b>
    PARTITION(year=<b>${year}</b>, month=<b>${month}</b>, day=<b>${day}</b>)
SELECT <mark data-annotation-id="3" aria-details="coalesce">/*+ COALESCE(1) */</mark>
    SUM(CAST(is_pageview AS INT)) AS pageviews
FROM
    <b>${source_table}</b>
WHERE
    x_analytics_map['<a href="https://wikitech.wikimedia.org/wiki/Provenance">wprov</a>'] REGEXP '^wppw(\\d+)(t?)$'
    AND agent_type = 'user'
    AND webrequest_source = 'text'
    AND year = <b>${year}</b>
    AND month = <b>${month}</b>
    AND day = <b>${day}</b>
;
          </code></pre>
          <p>
            Table-<i>populating</i> queries like that should <b>always</b> be accompanied by table-<i>creating</i> queries &mdash; for example: <a href="https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines/-/blob/main/wikipedia_preview/create_wikipedia_preview_stats_table.hql">create_wikipedia_preview_stats_table.hql</a>.
          </p>
        </div>
        <div class="content note">

          <div class="annotation" role="comment" data-annotation-id="1" id="usage">
            Effectively this is the command that Airflow generates and executes.
            It also demonstrates the values of the query's parameters.
          </div>
          <div class="annotation" role="comment" data-annotation-id="2" id="table-naming">
            As a best practice, do not hardcode table names in your query but make them parameters.
            This applies to both <code>source_table</code> and <code>destination_table</code>.
          </div>
          <div class="annotation" role="comment" data-annotation-id="3" id="coalesce">
            An instruction to Spark SQL to produce a single file within the partition.
          </div>

        </div>
      </section>

      <section class="group">
        <div class="content quote">
          <h2 id="data-dependencies">Data Dependencies<a href="#top" class="to-top" title="Navigate to the top">&uArr; Top</a></h2>
          <p>
            The file <a href="https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/analytics_product/config/datasets.yaml"><code>datasets.yaml</code></a>
            specifies all the source tables used by the queries to populate destination tables.
          </p>
          <pre><code class="language-yaml">
hive_wmf_webrequest_text:
  <mark data-annotation-id="1" aria-details="datastore"><b>datastore</b></mark>: hive
  <b>table_name</b> wmf.webrequest
  <mark data-annotation-id="2" aria-details="partitioning"><b>partitioning</b></mark>: "@hourly"
  <mark data-annotation-id="3" aria-details="pre-partitioning"><b>pre_partitions</b></mark>: ["webrequest_source=text"]
          </code></pre>
          <p>
            For more examples refer to
            <a href="https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/analytics/config/datasets.yaml">Data Engineering's datasets.yaml</a>.
          </p>
        </div>
        <div class="content note">
          <div class="annotation" role="comment" data-annotation-id="1" id="datastore">
            Specify: <code>druid</code>, <code>hive</code>, or <code>hive_snapshot</code> (for tables with "YYYY-MM"-formatted partitions). For example:
            <pre class="small">
hive_wmf_mediawiki_history:
  datastore: hive_snapshot
  table_name: wmf.mediawiki_history
  partitioning: "@monthly"</pre>
          </div>
          <div class="annotation" role="comment" data-annotation-id="2" id="partitioning">
            Use the predefined schedule intervals whenever possible (use <code>@daily</code> instead of <code>0 0 * * *</code>). For weekly DAGs, note the preset <code>@weekly</code> starts the DAGs on Sunday. If you want to start your DAG on Monday, use <code>0 0 * * 1</code>. If possible, all your weekly DAGs should start on the same day (either Sunday or Monday).
          </div>
          <div class="annotation" role="comment" data-annotation-id="3" id="pre-partitioning">
            In many cases you won't need to specify this, but it's necessary for the
            <a href="https://wikitech.wikimedia.org/wiki/Analytics/Data_Lake/Traffic/Webrequest">webrequest</a> table.
          </div>
        </div>
      </section>

      <section class="group">
        <div class="content quote">
          <h2 id="job-specification">Job Specification<a href="#top" class="to-top" title="Navigate to the top">&uArr; Top</a></h2>
          <p>
            The job is specified as an <a href="https://airflow.apache.org/docs/apache-airflow/stable/core-concepts/dags.html">Airflow DAG</a>
            (<a href="https://en.wikipedia.org/wiki/Directed_acyclic_graph">Directed Acyclic Graph</a>) with Python and it consists of multiple sections.
            The examples below are from <a href="https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/analytics_product/dags/wikipedia_preview/wikipedia_preview_stats_daily_dag.py"><code>wikipedia_preview_stats_daily_dag.py</code></a>. DAG filenames should be the same as DAG IDs, except they will have an <code>_dag.py</code> suffix.
          </p>
          <h3 id="preamble">Preamble<a href="#top" class="to-top" title="Navigate to the top">&uArr; Top</a></h3>
          <p>
            This where you document details about the job that will be displayed in the Airflow Web UI.
            You can use <a href="https://www.markdownguide.org/basic-syntax">basic Markdown syntax</a>.
          </p>
          <pre><code class="language-python">
"""
Computes wikipedia_preview_stats data daily.

* Waits for the corresponding webrequest partitions to be available.
* Populates the destination partition using a SparkSQL query file.
"""

from analytics_product.config.dag_config import alerts_email, dataset, create_easy_dag
from datetime import datetime, timedelta
from <mark aria-details="wmf-airflow-common">wmf_airflow_common</mark>.config.dag_properties import DagProperties
from wmf_airflow_common.operators.spark import SparkSqlOperator
          </code></pre>
          <p>The imports will be the same across the DAGs.</p>
        </div>
        <div class="content note">

          <div class="annotation" role="comment" id="wmf-airflow-common">
            You can find all the <code>wmf_airflow_common</code> modules
            <a href="https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/wmf_airflow_common">here</a>.
          </div>
        </div>
      </section>



      <section class="group">
        <div class="content quote">
          <h3 id="properties">Properties<a href="#top" class="to-top" title="Navigate to the top">&uArr; Top</a></h3>
          <p>
            The <a href="https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/wmf_airflow_common/config/dag_properties.py"><code>DagProperties</code></a>
            class helps to override DAG properties dynamically, altering them without changing the DAG file in version control. To achieve that, it uses <a href="https://airflow.apache.org/docs/apache-airflow/stable/core-concepts/variables.html">Airflow Variable</a>. Variables can be populated from Airflow's UI and also from the CLI. And they can be accessed from Airflow code (i.e. within a DAG file).

          </p>
          <pre><code class="language-python">
props = DagProperties(
  <mark data-annotation-id="1" aria-details="start-date"><b>start_date</b></mark>=datetime(2023, 5, 28),
  <mark data-annotation-id="2" aria-details="hql-uri"><b>hql_uri</b></mark>=(
      "https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines"
      "/-/raw/4c3e9fd235ad44b50839c2d8781f896cd73c4d0b"
      "/wikipedia_preview/generate_wikipedia_preview_stats_daily.hql"
  ),
  <mark data-annotation-id="3" aria-details="source-table"><b>webrequest_table</b>="wmf.webrequest"</mark>,
  <b>wikipedia_preview_stats_table</b>="wmf_product.wikipedia_preview_stats",
  <mark data-annotation-id="4" aria-details="sla"><b>sla</b>=timedelta(hours=6)</mark>,
  <mark data-annotation-id="5" aria-details="alerts-email"><b>alerts_email</b>=alerts_email</mark>
)
          </code></pre>
        </div>
        <div class="content note">
          <div class="annotation" role="comment" data-annotation-id="1" id="start-date">
            Remember, this is a property so it will end up being a variable you can modify in the Airflow web UI. For example, you might want to test a DAG and start it at a different date than the one intended for production. You also might want to back-fill starting i.e. at an earlier date.
          </div>
          <div class="annotation" role="comment" data-annotation-id="2" id="hql-uri">
            Use the "Permalink" button in the GitLab UI to navigate to the version of the file at the current state of the repository (based on the git commit sha).
            Then use the "Open raw" button to open the raw HQL file in your browser.
            Copy the URL in the address bar and use that as the <b>immutable URI</b> of the HQL file.
          </div>
          <div class="annotation" role="comment" data-annotation-id="3" id="source-table">
            Specifying the source &amp; destination table names here allows these to show up as editable variables in the Airflow UI, allowing you to temporarily change the references without having to modify the source code and deploy it. This is useful for testing DAGs because you can temporarily set the destination table to be a table in your personal database and then do a test run to verify that it's populated correctly.
          </div>
          <div class="annotation" role="comment" data-annotation-id="4" id="sla">
            Service Level Agreement. In practice, you set an SLA time on a Task.
            If the task instance has not finished after that time has elapsed, Airflow triggers an email alert.
          </div>
          <div class="annotation" role="comment" data-annotation-id="5" id="alerts-email">
            Defined in <a href="https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/analytics_product/config/dag_config.py"><code>dag_config.py</code></a>.
          </div>
        </div>
      </section>

      <section class="group">
        <div class="content quote">
          <h3 id="dag">DAG<a href="#top" class="to-top" title="Navigate to the top">&uArr; Top</a></h3>
          <p>
            The <a href="https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/analytics_product/config/dag_config.py"><code>create_easy_dag()</code></a>
            (<a href="https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/wmf_airflow_common/easy_dag.py">source</a>)
            helper function makes it much easier and simpler to pass parameters and configure a DAG.
          </p>
          <pre><code class="language-python">
with create_easy_dag(
  <mark data-annotation-id="1" aria-details="dag-id"><b>dag_id</b>="wikipedia_preview_stats_daily"</mark>,
  <b>doc_md</b>=<mark data-annotation-id="2" aria-details="documentation">__doc__</mark>,
  <b>start_date</b>=props.start_date,
  <b>schedule</b>="@daily",
  <b>sla</b>=props.sla,
  <b>email</b>=props.alerts_email
) as dag:

<mark data-annotation-id="3" aria-details="sensor">sensor = dataset("hive_wmf_webrequest_text").get_sensor_for(dag)</mark>

  compute = SparkSqlOperator(
    <mark data-annotation-id="4" aria-details="task-id"><b>task_id</b></mark>="compute_wikipedia_preview_stats",
    <mark data-annotation-id="5" aria-details="sql"><b>sql</b>=props.hql_uri</mark>,
    <mark data-annotation-id="6" aria-details="query-parameters">query_parameters</mark>={
        <b>"source_table"</b> props.webrequest_table,
        <b>"destination_table"</b> props.wikipedia_preview_stats_table,
        <b>"year"</b> "{{data_interval_start.year}}",
        <b>"month"</b> "{{data_interval_start.month}}",
        <b>"day"</b> "{{data_interval_start.day}}"
      },
      driver_cores=2,
      driver_memory="4G",
      executor_cores=4,
      executor_memory="8G",
      <mark data-annotation-id="7" aria-details="spark-config">conf</mark>={
          "spark.dynamicAllocation.maxExecutors": 16,
          "spark.yarn.executor.memoryOverhead": 2048
      }
  )

  <mark data-annotation-id="8" aria-details="sensor-compute">sensor &gt;&gt; compute</mark>
          </code></pre>
          <p>
            It's possible to add more tasks to the DAG.
            For example, suppose you have an ETL that aggregates event data from instrumentation and then have a Presto query in Superset that generates a virtual dataset.
            An unfortunate quirk of virtual datasets in Supersets is that the results aren't shared between different charts using that data &ndash; each chart runs an identical query independent of the other charts.
            In some cases you may end up with many or all of your charts timing out. What you can do there is offload the computations to another ETL and build a chain.
            The task chain might look like <code>sensor &gt;&gt; aggregate_low_level &gt;&gt; aggregate_high_level &gt;&gt; ingest_aggregates_into_druid</code>
          </p>
        </div>
        <div class="content note">
          <div class="annotation" role="comment" data-annotation-id="1" id="dag-id">
            <p>The DAG identifier needs to use <code>snake_case</code> and needs to be unique across the entire Airflow instance.</p>
            <p>
              Some ideas of what a DAG id could contain: The name of the resulting dataset, the granularity of the job (if there are several jobs with different granularities for the same dataset), and the tool that the DAG uses to process or store the resulting dataset (i.e. druid or anomaly_detection).
              Avoid very generic words like "data", "table" or "dag", which don't give much information.
            </p>
          </div>
          <div class="annotation" role="comment" data-annotation-id="2" id="documentation">
            This pulls the documentation from the top of the file. (Refer to the <a href="#preamble">preamble section</a> above.)
          </div>
          <div class="annotation" role="comment" data-annotation-id="3" id="sensor">
            A <i>sensor</i> is a task that waits for a particular asset to be present in the specified location.
            For instance, it waits for a Hive partition to exist, or an HDFS path to exist.
            It references the dependency defined in datasets.yaml. (Refer to the <a href="#data-dependencies">data dependencies section</a> above.)
          </div>
          <div class="annotation" role="comment" data-annotation-id="4" id="task-id">
            <p>The task identifier needs to use <code>snake_case</code> and needs to be unique within the DAG.</p>
            <p>Task IDs should describe actions (i.e. "process_certain_dataset"), if possible starting with a verb and following with an object. It is a good idea to be explicit about which data is the object of the action.</p>
          </div>
          <div class="annotation" role="comment" data-annotation-id="5" id="sql">
            Retrieving the HQL file URI from the DAG properties &ndash; rather than setting it here &ndash; allows you to temporarily change it in the Airflow web UI (since it would be a variable).
          </div>
          <div class="annotation" role="comment" data-annotation-id="6" id="query-parameters">
            This is where we pass the appropriate values to the query's parameters. (Refer to the <a href="#query">query section</a> above.)
          </div>
          <div class="annotation" role="comment" data-annotation-id="7" id="spark-config">
            The <a href="https://github.com/wikimedia/wmfdata-python/blob/main/wmfdata/spark.py"><code>wmfdata.spark</code></a> module has some Spark session configurations you can reference (<code>yarn-regular</code> &amp; <code>yarn-large</code>).
          </div>
          <div class="annotation" role="comment" data-annotation-id="8" id="sensor-compute">
            This establishes task execution order. Until the <code>sensor</code> confirms the presence of its target, the subsequent tasks (<code>compute</code>) in the DAG aren't triggered.
          </div>
        </div>
      </section>

      <section class="group last">
        <div class="content quote">
          <h2 id="unit-test">Unit Test<a href="#top" class="to-top" title="Navigate to the top">&uArr; Top</a></h2>
          <p>
            Unlike the other files (datasets.yaml and the DAGs) which are under the
            <a href="https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/analytics_product"><code>analytics_product</code></a> sub-directory in
            <a href="https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags"><code>repos/data-engineering/airflow-dags</code></a>,
            the unit tests for the jobs are under
            <a href="https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/tree/main/tests/analytics_product"><code>tests/analytics_product</code></a>.
            The example below is from
            <a href="https://gitlab.wikimedia.org/repos/data-engineering/airflow-dags/-/blob/main/tests/analytics_product/wikipedia_preview/wikipedia_preview_stats_daily_dag_test.py"><code>wikipedia_preview_stats_daily_dag_test.py</code></a>. The tests check that the DAG parses without errors and then checks how many tasks are defined.
          </p>
          <pre><code class="language-python">
import pytest

@pytest.fixture(name='dag_path')
def fixture_dagpath():
    return ['analytics_product', 'dags', 'wikipedia_preview',
        'wikipedia_preview_stats_daily_dag.py']

def test_wikipedia_preview_stats_daily_dag(dagbag):
    assert dagbag.import_errors == {}
    dag = dagbag.get_dag(dag_id='wikipedia_preview_stats_daily')
    assert dag is not None
    assert <mark aria-details="dag-tasks">len(dag.tasks) == 2</mark>
          </code></pre>
        </div>
        <div class="content note">
          <div class="annotation" role="comment" id="dag-tasks">
            The example DAG in this guide has 2 tasks: <code>sensor</code> and <code>compute</code>.
          </div>
        </div>
      </section>

    </article>
  </main>
  <footer class="outro">
    <div class="fine-print">
      <p>
        <a href="https://gitlab.wikimedia.org/repos/product-analytics/data-pipelines-guide">Source code</a> &copy; Wikimedia Foundation, licensed under
        <a href="https://www.apache.org/licenses/LICENSE-2.0">Apache License, Version 2.0</a>.
        Text is available under the
        <a href="https://creativecommons.org/licenses/by-sa/4.0/deed.en">Creative Commons Attribution-ShareAlike License</a>.
        Page built with
        <a href="https://github.com/molly/annotate" target="_blank">Annotate</a>.
      </p>
    </div>
  </footer>
</body>

</html>
